from __future__ import division

__author__ = 'Polina'

import turtle
import math

color_range = ['#808000', '#6B8E23', '#FFA500', '#FF4500', '#FA8072', '#F4A460', '#BC8F8F']
color = 0
radius = 200
dct = {}

def draw_pie_chart(chart, sentence):
    words, word_number = split_sentence(sentence)
    draw_pie(chart, word_number, radius)
    chart.up()
    chart.setposition(radius+50, radius+50)
    chart.down()
    write_statistics(chart, words)

def write_statistics(chart, words):
    global color
    color = 0
    chart.pensize(1)
    chart.shape('circle')
    for word in words:
        start_x = chart.xcor()
        start_y = chart.ycor()
        change_color(chart)
        chart.stamp()
        chart.setposition(start_x+30, start_y)
        string = str(word) + ' - ' + str(words[word]) + ' time(-s)'
        chart.write(string, 'left')
        chart.up()
        chart.setposition(start_x, start_y-50)
        chart.down()
    chart.hideturtle()


def draw_pie(chart, word_number, radius):
    chart.pensize(5)
    start_angle = 0
    for word in dct:
        part_angle = dct[word]/word_number*360
        change_color(chart)
        current_angle = start_angle
        end_angle = start_angle + part_angle
        chart.begin_fill()
        while current_angle <= end_angle:
            chart.setposition(radius*math.cos(math.radians(current_angle)), radius*math.sin(math.radians(current_angle)))
            current_angle += 0.3
        chart.setposition(0, 0)
        chart.end_fill()
        start_angle += part_angle

def split_sentence(sentence):
    global dct
    dct = {}
    lst = sentence.split()
    for word in lst:
        dct[word] = dct[word] + 1 if word in dct else 1
    return dct, len(lst)


def change_color(figure):
    global color
    figure.color(color_range[color], color_range[color])
    color += 1
    if color == len(color_range):
        color = 1

def draw_ray_chart(chart, sentence):
    chart.pensize(3)
    words, word_number = split_sentence(sentence)
    angle = 360 / len(words)
    for word in words:
        change_color(chart)
        for i in xrange(0, words[word]):
            chart.forward(radius/2)
            chart.stamp()
        chart.write(word)
        chart.setposition(0, 0)
        chart.left(angle)



def init(sentence, method):
    wn = turtle.Screen()
    diagram = turtle.Turtle()
    diagram.speed(0)

    if method.lower() == 'pie':
        draw_pie_chart(diagram, sentence)
    elif method.lower() == 'ray':
        draw_ray_chart(diagram, sentence)
    else:
        print 'Try again!'
    wn.exitonclick()

if __name__ == "__main__":
    init(raw_input('Please enter a sentence \n').lower(), raw_input("Enter 'pie' for a Pie chart  and 'ray' for a Ray "
                                                                    "chart \n").lower())

