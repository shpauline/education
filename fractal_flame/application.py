from __future__ import division

__author__ = 'Polina'

import os
import json
import random
import string
import math as m
from PIL import Image, ImageDraw


import canvas
import transformation
import coefficient


class Application(object):
    factor_amount = 16
    x_min = -1.777
    x_max = 1.777
    y_min = -1.0
    y_max = 1.0
    gamma = 2.0
    symmetry = 1
    start_index = 0

    def __init__(self):

        self.coefficients = []
        self.file_name = ''
        self.choice = -1
        self.rand_factor = None
        self.new_x = 0
        self.new_y = 0
        self.x_rot = 0
        self.y_rot = 0
        self.theta = 0
        self.current_pixel = None
        self.range_x = self.x_max - self.x_min
        self.range_y = self.y_max - self.y_min
        self.canvas = None
        self.randomize = None

        self.transformations = [transformation.Transformation.linear,
                                transformation.Transformation.spherical,
                                transformation.Transformation.swirl,
                                transformation.Transformation.handkerchief,
                                transformation.Transformation.heart,
                                transformation.Transformation.disk,
                                transformation.Transformation.spiral,
                                transformation.Transformation.hyperbolic,
                                transformation.Transformation.diamond,
                                transformation.Transformation.ex,
                                transformation.Transformation.cylinder,
                                transformation.Transformation.tangent,
                                transformation.Transformation.cross
                                ]

    def way_choosing(self):
        choice = raw_input('Do you want to load the existing parameters? (y/n)\n').lower()
        if choice == 'y':
            self.load_parameters()
        elif choice == 'n':
            self.create_canvas()
        else:
            print '''Please enter 'y' or 'n'.'''
            self.way_choosing()

    def load_parameters(self):
        parameters = self.open_json_file()
        if parameters is not None:
            self.canvas = canvas.Canvas(x_res=parameters['x_res'], y_res=parameters['y_res'],
                                        samples=parameters['samples'], iterations=parameters['iterations'],
                                        seed=parameters['seed'])

            for parameter in parameters['coefficients']:
                self.coefficients.append(coefficient.Coefficient(**parameter))
            self.randomize = random.Random(self.canvas.seed)
            self.choice = parameters['transformation_choice']

    def open_json_file(self):
        print 'The list of existing files: '
        files = os.listdir("./json_files/")
        json_files = filter(lambda x: x.endswith('.json'), files)
        for i in xrange(len(json_files)):
            print json_files[i]
        try:
            file_name = raw_input('Please enter the file name (without a resolution)\n')
            with open('./json_files/' + file_name + '.json', 'r') as infile:
                data = json.load(infile)
            self.file_name = file_name
        except IOError:
            print 'There is no such file! Try again!\n'
            self.open_json_file()
            return None
        else:
            print 'File was successfully opened.\n'
            return data

    def create_canvas(self):
        self.canvas = canvas.Canvas()
        self.randomize = random.Random(self.canvas.seed)
        self.coefficient_initialization()

    def coefficient_initialization(self):
        for _ in xrange(self.factor_amount):
            self.coefficients.append(coefficient.Coefficient())
        for i, factor in enumerate(self.coefficients):
            factor.index = i

    def factor_choosing(self):
        factor_number = self.randomize.randrange(self.factor_amount)
        self.rand_factor = self.coefficients[factor_number]
        x = self.rand_factor.a * self.new_x + self.rand_factor.b * self.new_y + self.rand_factor.c
        y = self.rand_factor.d * self.new_x + self.rand_factor.e * self.new_y + self.rand_factor.f
        self.new_x, self.new_y = self.transformation_choice(x, y)

    def rotation_choosing(self, theta):
        self.theta += (2 * m.pi / self.symmetry)
        self.x_rot = self.new_x * m.cos(theta) - self.new_y * m.sin(theta)
        self.y_rot = self.new_x * m.sin(theta) + self.new_y * m.cos(theta)

    def set_factor_channel(self):
        self.current_pixel.r_channel = self.rand_factor.r_channel
        self.current_pixel.g_channel = self.rand_factor.g_channel
        self.current_pixel.b_channel = self.rand_factor.b_channel

    def set_middle_channel(self):
        self.current_pixel.r_channel = (self.rand_factor.r_channel + self.current_pixel.r_channel) / 2
        self.current_pixel.g_channel = (self.rand_factor.g_channel + self.current_pixel.g_channel) / 2
        self.current_pixel.b_channel = (self.rand_factor.b_channel + self.current_pixel.b_channel) / 2

    def generation(self):
        for i in xrange(0, self.canvas.samples):
            self.new_x = random.randrange(self.canvas.x_res)
            self.new_y = random.randrange(self.canvas.y_res)
            for j in xrange(-20, self.canvas.iterations):
                self.factor_choosing()
                if i > 0:
                    self.theta = 0
                    for t in xrange(0, self.symmetry):
                        self.rotation_choosing(self.theta)
                        if self.x_max > self.x_rot > self.x_min and self.y_max > self.y_rot > self.y_min:
                            x1 = int(self.canvas.x_res - (self.x_max - self.x_rot) / self.range_x * self.canvas.x_res)
                            y1 = int(self.canvas.y_res - (self.y_max - self.y_rot) / self.range_y * self.canvas.y_res)
                            if 0 <= x1 < self.canvas.x_res and 0 <= y1 < self.canvas.y_res:
                                self.current_pixel = self.canvas.pixels[x1][y1]
                                if self.current_pixel.count == 0:
                                    self.set_factor_channel()
                                else:
                                    self.set_middle_channel()
                                self.current_pixel.count += 1

    def gamma_conversion(self):
        max_normal = 0.0
        for row in self.canvas.pixels:
            for current_pixel in row:
                if current_pixel.count != 0:
                    current_pixel.normal = m.log10(current_pixel.count)
                    if current_pixel.normal > max_normal:
                        max_normal = current_pixel.normal

        for row in self.canvas.pixels:
            for current_pixel in row:
                current_pixel.r_channel *= (current_pixel.normal / max_normal) ** (1 / self.gamma)
                current_pixel.g_channel *= (current_pixel.normal / max_normal) ** (1 / self.gamma)
                current_pixel.b_channel *= (current_pixel.normal / max_normal) ** (1 / self.gamma)

    def get_name(self):
        self.file_name = self.transformations[self.choice].__name__ + '_' + \
            ''.join(random.sample(string.ascii_uppercase + string.ascii_lowercase + string.digits, 10))

    def transformation_choice(self, x, y):
        if self.choice == -1:
            print 'The list of available transformations:'
            for index, transform in enumerate(self.transformations):
                print index + 1, '-', transform.__name__
            choice = int(raw_input('Please enter a number of transformation you want to perform:\n')) - 1
            if 0 <= choice < len(self.transformations):
                self.choice = choice
                return self.transformations[self.choice](x, y)
            else:
                print 'Try again!'
                self.transformation_choice(x, y)
        else:
            return self.transformations[self.choice](x, y)

    def draw_image(self):
        image = Image.new('RGB', (self.canvas.x_res, self.canvas.y_res))
        draw = ImageDraw.Draw(image)

        for i in xrange(self.canvas.x_res):
            for j in xrange(self.canvas.y_res):
                draw.point((i, j), self.canvas.pixels[i][j].get_color())
        self.get_name()
        print "Your file's name is", self.file_name + '.jpeg'
        try:
            image.save('./images/' + self.file_name, 'JPEG')
        except IOError:
            os.mkdir('./images')
            image.save('./images/' + self.file_name, 'JPEG')

    def save_parameters(self):
        coefficient_array = list()
        for factor in self.coefficients:
            coefficient_array.append(factor.give_parameters())
        data = {
            'seed': self.canvas.seed,
            'factor_amount': self.factor_amount,
            'x_res': self.canvas.x_res,
            'y_res': self.canvas.y_res,
            'samples': self.canvas.samples,
            'x_min': self.x_min,
            'x_max': self.x_max,
            'y_min': self.y_min,
            'y_max': self.y_max,
            'gamma': self.gamma,
            'iterations': self.canvas.iterations,
            'symmetry': self.symmetry,
            'coefficients': coefficient_array,
            'transformation_choice': self.choice

        }
        try:
            with open('./json_files/' + self.file_name + '.json', 'w') as outfile:
                json.dump(data, outfile)
        except IOError:
            os.mkdir('./json_files')
            with open('./json_files/' + self.file_name + '.json', 'w') as outfile:
                json.dump(data, outfile)

    def run(self):
        self.way_choosing()
        self.generation()
        self.gamma_conversion()
        self.draw_image()
        self.save_parameters()
