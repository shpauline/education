__author__ = 'Polina'

import random

import pixel

class Canvas(object):
    x_res = 1000
    y_res = 500
    samples = 2000
    iterations = 1000
    seed = random.randrange(50)

    def __init__(self, **kwargs):
        for parameter in kwargs:
            self.__setattr__(parameter, kwargs[parameter])
        self.pixels = [[pixel.Pixel() for _ in xrange(self.y_res)] for _ in xrange(self.x_res)]
