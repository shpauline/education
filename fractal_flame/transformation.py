from __future__ import division

__author__ = 'Polina'

import math as m


class Transformation(object):
    @staticmethod
    def linear(x, y):
        new_x = x
        new_y = y
        return new_x, new_y

    @staticmethod
    def spherical(x, y):
        r = 1.0 / (x * x + y * y)
        new_x = r * x
        new_y = r * y
        return new_x, new_y

    @staticmethod
    def swirl(x, y):
        r = x * x + y * y
        new_x = x * m.sin(r) - y * m.cos(r)
        new_y = x * m.cos(r) + y * m.sin(r)
        return new_x, new_y

    @staticmethod
    def handkerchief(x, y):
        r = m.sqrt(x * x + y * y)
        theta = m.atan2(y, x)
        new_x = r * m.sin(theta + r)
        new_y = r * m.cos(theta - r)
        return new_x, new_y

    @staticmethod
    def heart(x, y):
        r = m.sqrt(x * x + y * y)
        theta = m.atan2(y, x)
        new_x = r * m.sin(theta * r)
        new_y = -r * m.cos(theta * r)
        return new_x, new_y

    @staticmethod
    def disk(x, y):
        r = m.sqrt(x * x + y * y) * m.pi
        theta = m.atan2(y, x) / m.pi
        new_x = theta * m.sin(r)
        new_y = theta * m.cos(r)
        return new_x, new_y

    @staticmethod
    def spiral(x, y):
        r = m.sqrt(x * x + y * y)
        theta = m.atan2(y, x)
        new_x = (1.0 / r) * (m.cos(theta) + m.sin(r))
        new_y = (1.0 / r) * (m.sin(theta) - m.cos(r))
        return new_x, new_y

    @staticmethod
    def hyperbolic(x, y):
        r = m.sqrt(x * x + y * y)
        theta = m.atan2(y, x)
        new_x = m.sin(theta) / r
        new_y = r * m.cos(theta)
        return new_x, new_y

    @staticmethod
    def diamond(x, y):
        r = m.sqrt(x * x + y * y)
        theta = m.atan2(y, x)
        new_x = m.sin(theta) * m.cos(r)
        new_y = m.cos(theta) * m.sin(r)
        return new_x, new_y

    @staticmethod
    def ex(x, y):
        r = m.sqrt(x * x + y * y)
        theta = m.atan2(y, x)
        p0 = m.sin(theta + r)
        p0 **= 3
        p1 = m.cos(theta - r)
        p1 **= 3
        new_x = r * (p0 + p1)
        new_y = r * (p0 - p1)
        return new_x, new_y

    @staticmethod
    def cylinder(x, y):
        new_x = m.sin(x)
        new_y = y
        return new_x, new_y

    @staticmethod
    def tangent(x, y):
        new_x = m.sin(x)/m.cos(y)
        new_y = m.tan(y)
        return new_x, new_y

    @staticmethod
    def cross(x, y):
        r = m.sqrt(1.0 / ((x * x - y * y) * (x * x - y * y)))
        new_x = x * r
        new_y = y * r
        return new_x, new_y

