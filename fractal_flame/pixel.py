__author__ = 'Polina'

hex_chars = '0123456789ABCDEF'

class Pixel:
    def __init__(self):
        self.r_channel = self.g_channel = self.b_channel = 0
        self.count = 0
        self.normal = 0

    def get_color(self):
        self.r_channel = int(self.r_channel)
        self.g_channel = int(self.g_channel)
        self.b_channel = int(self.b_channel)
        return '#' + hex_chars[self.r_channel // 16] + hex_chars[self.r_channel % 16] + hex_chars[self.g_channel // 16] + \
               hex_chars[self.g_channel % 16] + hex_chars[self.b_channel // 16] + hex_chars[self.b_channel % 16]
