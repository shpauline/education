__author__ = 'Polina'

import random

base_width = 5
smallest = 10
largest = 25

def max_path(pyramid):
    new_pyramid = [row[:] for row in pyramid]
    for i in xrange(len(new_pyramid) - 2, -1, -1):
        for j in xrange(i + 1):
            new_pyramid[i][j] += max(new_pyramid[i + 1][j], new_pyramid[i + 1][j + 1])
    return new_pyramid[0][0]

def print_pyramid(width, pyramid):
    for i in xrange(width):
        print ' '*(width*3 - len(pyramid[i])*2), pyramid[i]

def create_pyramid(width):
    generated_pyramid = [[random.randrange(smallest, largest) for _ in xrange(i + 1)] for i in xrange(width)]
    return generated_pyramid

if __name__ == "__main__":
    pyramid = create_pyramid(base_width)
    print_pyramid(base_width, pyramid)
    print '\nThe maximum sum is:', max_path(pyramid)
