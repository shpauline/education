__author__ = 'Polina'
  
import turtle
import random

base_color = '#008B8B'
erase_color = '#00FFFF'
angle = 120
sides = 3
dot_number = 1000

def iter_method(triangle, x_start=-250, y_start=-250, length=500, sep_level=4):
    x_cor = []
    y_cor = []
    triangle.up()
    triangle.setposition(x_start, y_start)
    triangle.down()
    triangle.color(base_color, base_color)
    triangle.begin_fill()
    for i in xrange(sides):
        x_cor.append(triangle.xcor())
        y_cor.append(triangle.ycor())
        triangle.forward(length)
        triangle.left(angle)
    triangle.end_fill()
    triangle.color(erase_color, erase_color)
    triangle.left(-angle/2)
    sep_triangle(triangle, x_cor[0], y_cor[0], x_cor[2], y_cor[2], length, sep_level)
    triangle.right(angle)
    sep_triangle(triangle, x_cor[1], y_cor[1], x_cor[2], y_cor[2], length, sep_level)
    triangle.right(angle)
    sep_triangle(triangle, x_cor[0], y_cor[0], x_cor[1], y_cor[1], length, sep_level)

def sep_triangle(triangle, x1, y1, x2, y2, length, sep_level):
    x_cor = []
    y_cor = []
    triangle.up()
    triangle.setposition((x1+x2) / 2, (y1+y2) / 2)
    triangle.down()
    triangle.begin_fill()
    for i in xrange(sides):
        x_cor.append(triangle.xcor())
        y_cor.append(triangle.ycor())
        triangle.forward(length/2)
        triangle.left(angle)
    triangle.end_fill()
    for num in xrange(1, sep_level):
        sep_triangle(triangle, x_cor[1], y_cor[1], x_cor[2], y_cor[2],  length/2, num)
        sep_triangle(triangle, x_cor[0], y_cor[0], x_cor[1], y_cor[1],  length/2, num)
        sep_triangle(triangle, x_cor[0], y_cor[0], x_cor[2], y_cor[2],  length/2, num)

def chaos_method(triangle, x0=-250, y0=-250, x1=250, y1=-250, x2=0, y2=183, point_x=random.randrange(-800, 800, 5),
                 point_y=random.randrange(-800, 800, 5)):
    triangle.color(base_color)
    triangle.up()
    triangle.setposition(point_x, point_y)
    triangle.down()
    for i in xrange(dot_number):
        point = random.randrange(0, 3, 1)
        triangle.up()
        triangle.up()
        if point == 0:
            mid = get_mid(triangle.xcor(), triangle.ycor(), x0, y0)
        elif point == 1:
            mid = get_mid(triangle.xcor(), triangle.ycor(), x1, y1)
        else:
            mid = get_mid(triangle.xcor(), triangle.ycor(), x2, y2)
        triangle.setposition(mid[0], mid[1])
        triangle.down()
        triangle.dot(7)

def get_mid(x1, y1, x2, y2):
    return [(x1 + x2) / 2, (y1 + y2) / 2]

def init(input):
    wn = turtle.Screen()
    wn.bgcolor('#F5F5F5')
    sierpinski = turtle.Turtle()
    sierpinski.speed(0)
    if input.lower() == 'yes':
        iter_method(sierpinski)
    elif input.lower() == 'no':
        chaos_method(sierpinski)
    else:
        print 'Try again!'
    wn.exitonclick()

if __name__ == "__main__":
    init(raw_input("Enter 'yes' for Iterative method and 'no' for Chaos one \n"))

