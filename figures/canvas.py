__author__ = 'Polina'

import validity_check

class Canvas(object):
    def __init__(self):
        self.window_width = 0
        self.window_height = 0
        self.window_color = 'white'
        self.figures = []

    def set_size(self):
        wn_width = int(raw_input('Please enter the width of the window: \n'))
        wn_height = int(raw_input('Please enter the height of the window: \n'))
        if validity_check.Validation.is_correct(wn_width, wn_height):
            self.window_width = wn_width
            self.window_height = wn_height
        else:
            print 'The values are incorrect! Try again!'
            self.set_size()

    def add_figure(self, figure):
        self.figures.append(figure)

    def get_figures(self):
        if len(self.figures) == 0:
            print 'The list is empty!'
            return False
        else:
            for figure in xrange(len(self.figures)):
                print 'Figure', figure + 1
                self.figures[figure].display()
            return True

    def set_color(self):
        print 'Current canvas color is', self.window_color
        choice = raw_input('Do you want to change the canvas color? (y/n)\n').lower()
        if choice == 'y':
            self.window_color = raw_input('Please enter the color: \n')
        elif choice == 'n':
            pass
        else:
            print '''Please enter 'y' or 'n'. \n'''
            self.set_color()

    def set_figures(self, figure_information, type):
        figure = type()
        figure_information.pop('figure_type')
        figure.set_information(**figure_information)
        self.figures.append(figure)
