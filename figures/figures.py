from __future__ import division

__author__ = 'Polina'

import gtk
from turtle import TurtleGraphicsError as TurtleError
import abc

import validity_check


class Figure(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        self.start_x = 0
        self.start_y = 0
        self.color = ['black', 'black']
        self.filling = False
        self.pen_size = 1

    def change_parameters(self):
        new_x = int(raw_input('Please enter X starting coordinate: \n'))
        new_y = int(raw_input('Please enter Y starting coordinate: \n'))
        if validity_check.Validation.is_in_area(new_x, new_y):
            self.start_x = new_x
            self.start_y = new_y
            self.change_color()
            self.change_pen_size()
        else:
            print 'Too large values! Try again!'
            self.change_parameters()

    def change_color(self):
        pencolor = raw_input('Please enter a pen color: \n').lower()
        self.filling_mode()
        if self.filling:
            fillcolor = raw_input('Please enter a color of filling: \n').lower()
            self.color = [pencolor, fillcolor]
        else:
            self.color = [pencolor, 'black']

    def filling_mode(self):
        mode = raw_input('Do you want to fill a figure? (y/n)\n').lower()
        if mode == 'y':
            self.filling = True
        elif mode == 'n':
            self.filling = False
        else:
            print '''Please enter 'y' or 'n'. \n'''
            self.filling_mode()

    def change_pen_size(self):
        pen_size = int(raw_input('Please enter the size of a pen: \n'))
        if validity_check.Validation.length_check(pen_size):
            self.pen_size = pen_size

    def draw_figure(self, turtle):
        try:
            turtle.color(self.color[0], self.color[1])
        except TurtleError:
            turtle.color('black', 'black')
        else:
            turtle.width(self.pen_size)
            turtle.up()
            turtle.setposition(self.start_x, self.start_y)
            turtle.down()

    @abc.abstractmethod
    def display(self):
        pass

    @abc.abstractmethod
    def give_information(self):
        pass

    @abc.abstractmethod
    def set_information(self, *args):
        pass


class Polygon(Figure):
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        super(Polygon, self).__init__()
        self.angles_number = 0

    def change_parameters(self):
        super(Polygon, self).change_parameters()
        angles_number = int(raw_input('Please enter the number of angles: \n'))
        if validity_check.Validation.angle_check(angles_number):
            self.angles_number = angles_number
        else:
            print 'Try again!'
            self.change_parameters()


class RPolygon(Polygon):
    def __init__(self):
        super(RPolygon, self).__init__()
        self.side_length = 0

    def change_parameters(self):
        super(RPolygon, self).change_parameters()
        side_length = int(raw_input('Please enter the side length: \n'))
        if validity_check.Validation.length_check(side_length):
            self.side_length = side_length
        else:
            print 'Try again!'
            self.change_parameters()

    def display(self):
        print 'Regular Polygon. Start X:', self.start_x, 'Start Y:', self.start_y, 'Number of angles:', \
            self.angles_number, 'Side length:', self.side_length

    def draw_figure(self, turtle):
        super(RPolygon, self).draw_figure(turtle)
        if self.filling:
            turtle.begin_fill()
        for i in xrange(self.angles_number):
            turtle.forward(self.side_length)
            turtle.left(360 / self.angles_number)
        if self.filling:
            turtle.end_fill()

    def give_information(self):
        return {'figure_type': 'RPolygon', 'start_x': self.start_x, 'start_y': self.start_y,
                'figure_color': self.color, 'filling_mode': self.filling, 'pen_size': self.pen_size,
                'angles_number': self.angles_number, 'side_length': self.side_length}

    def set_information(self, start_x, start_y, figure_color, filling_mode, pen_size, angles_number, side_length):
        self.start_x = start_x
        self.start_y = start_y
        self.color = figure_color
        self.filling = filling_mode
        self.pen_size = pen_size
        self.angles_number = angles_number
        self.side_length = side_length

class IPolygon(Polygon):
    def __init__(self):
        super(IPolygon, self).__init__()
        self.coordinates = [[] for _ in xrange(self.angles_number)]

    def change_parameters(self):
        super(IPolygon, self).change_parameters()
        del self.coordinates[:]
        self.coordinates.append([self.start_x, self.start_y])
        print 'Please enter the coordinates'
        for i in xrange(1, self.angles_number):
            self.coordinates.append([int(raw_input(str(i + 1) + ' dot\nX\n' if j == 0 else 'Y\n')) for j in xrange(2)])

    def display(self):
        print 'Irregular Polygon. Start X:', self.start_x, 'Start Y:', self.start_y, 'Number of angles:', \
            self.angles_number, 'Angle coordinates:', self.coordinates

    def draw_figure(self, turtle):
        super(IPolygon, self).draw_figure(turtle)
        if self.filling:
            turtle.begin_fill()
        for i in xrange(1, self.angles_number):
            turtle.setposition(self.coordinates[i][0], self.coordinates[i][1])
        turtle.setposition(self.coordinates[0][0], self.coordinates[0][1])
        if self.filling:
            turtle.end_fill()

    def give_information(self):
        return {'figure_type': 'IPolygon', 'start_x': self.start_x, 'start_y': self.start_y,
                'figure_color': self.color, 'filling_mode': self.filling, 'pen_size': self.pen_size,
                'angles_number': self.angles_number, 'coordinates': self.coordinates}

    def set_information(self, start_x, start_y, figure_color, filling_mode, pen_size, angles_number, coordinates):
        self.start_x = start_x
        self.start_y = start_y
        self.color = figure_color
        self.filling = filling_mode
        self.pen_size = pen_size
        self.angles_number = angles_number
        self.coordinates = coordinates

class Circle(Figure):
    def __init__(self):
        super(Circle, self).__init__()
        self.radius = 0

    def change_parameters(self):
        super(Circle, self).change_parameters()
        radius = int(raw_input('Please enter the radius of the circle: \n'))
        if validity_check.Validation.length_check(radius):
            self.radius = radius
        else:
            print 'Try again!'
            self.change_parameters()

    def display(self):
        print 'Circle. Start X:', self.start_x, 'Start Y:', self.start_y, 'Radius:', self.radius

    def draw_figure(self, turtle):
        super(Circle, self).draw_figure(turtle)
        if self.filling:
            turtle.begin_fill()
        turtle.circle(self.radius)
        if self.filling:
            turtle.end_fill()

    def give_information(self):
        return {'figure_type': 'Circle', 'start_x': self.start_x, 'start_y': self.start_x,
                'figure_color': self.color, 'filling_mode': self.filling, 'pen_size': self.pen_size,
                'radius': self.radius}

    def set_information(self, start_x, start_y, figure_color, filling_mode, pen_size, radius):
        self.start_x = start_x
        self.start_y = start_y
        self.color = figure_color
        self.filling = filling_mode
        self.pen_size = pen_size
        self.radius = radius
