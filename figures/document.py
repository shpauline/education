__author__ = 'Polina'

import random
import string
import json
from os import listdir

class Document(object):
    def __init__(self):
        self.status = None
        self.file_name = ''

    def get_name(self):
        choice = raw_input('Do you want to choose a name for this document? (y/n)\n').lower()
        if choice == 'y':
            self.file_name = raw_input('Please enter the name (without a resolution): \n')
        elif choice == 'n':
            self.file_name = ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits)
                                     for _ in xrange(12))
            print 'The file name is', self.file_name, '\n'
        else:
            print '''Please enter 'y' or 'n'. \n'''
            self.get_name()

    def change_status(self, status):
        self.status = status

    def check_status(self):
        return False if self.status is None else True

    def save_file(self, canvas_width, canvas_height, canvas_color, figures):
        data = {'canvas_width': canvas_width, 'canvas_height': canvas_height, 'canvas_color': canvas_color,
                'figures': figures}
        with open(self.file_name + '.json', 'w') as outfile:
            json.dump(data, outfile)

    def open_file(self):
        print 'The list of existing files: '
        files = listdir(".")
        json_files = filter(lambda x: x.endswith('.json'), files)
        for i in xrange(len(json_files)):
            print json_files[i]
        try:
            file_name = raw_input('Please enter the file name (without a resolution)\n')
            with open(file_name + '.json', 'r') as infile:
                data = json.load(infile)
            self.change_status('Opened')
            self.file_name = file_name
        except IOError:
            print 'There is no such file! Try again!\n'
            return None
        else:
            print 'File was successfully opened.\n'
            return data
