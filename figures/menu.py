__author__ = 'Polina'
import gtk
import turtle
from turtle import TurtleGraphicsError as TurtleError

import figures
import document
import canvas
import validity_check


class Application(object):
    def __init__(self):
        print '''Hello! This is console graphics editor'''
        self.document = document.Document()
        self.canvas = canvas.Canvas()
        self.turtle = None
        self.commands = {'1': self.create_document,
                         '2': self.open_document,
                         '3': self.save_document,
                         '4': self.add_figure,
                         '5': self.del_figure,
                         '6': self.get_figure_list,
                         '7': self.change_figure_parameters,
                         '8': self.build_image,
                         'one': self.create_document,
                         'two': self.open_document,
                         'three': self.save_document,
                         'four': self.add_figure,
                         'five': self.del_figure,
                         'six': self.get_figure_list,
                         'seven': self.change_figure_parameters,
                         'eight': self.build_image
                         }

    def choice(self):
        command = raw_input('Please enter a number of the command to execute: \n'
                            '1. Create a new document \n'
                            '2. Open a document \n'
                            '3. Save a document \n'
                            '4. Add a figure \n'
                            '5. Delete a figure \n'
                            '6. Get a list of figures \n'
                            '7. Change figure parameters \n'
                            '8. Build an image \n'
                            '9. Exit \n').lower()
        self.menu(command)

    def create_document(self):
        if not self.document.check_status():
            self.document.change_status('New')
            self.canvas.set_size()
        else:
            choice = raw_input('The current information will be lost.\nAre you sure you want to continue? (y/n)\n').\
                lower()
            if choice == 'y':
                self.document.change_status('New')
                self.canvas.set_size()
                self.canvas.window_color = 'white'
                del self.canvas.figures[:]
            elif choice == 'n':
                pass
            else:
                print '''Please enter 'y' or 'n'. \n'''

    def open_document(self):
        if not self.document.check_status():
            data = self.document.open_file()
            if data is not None:
                self.canvas.window_width = data['canvas_width']
                self.canvas.window_height = data['canvas_height']
                self.canvas.window_color = data['canvas_color']
                figures_information = data['figures']
                del self.canvas.figures[:]
                for i in xrange(len(figures_information)):
                    self.canvas.set_figures(figures_information[i],
                                            figures.__dict__[figures_information[i]['figure_type']])
            else:
                pass
        else:
            choice = raw_input('The current information will be lost.\nAre you sure you want to continue? (y/n)\n').\
                lower()
            if choice == 'y':
                self.document.change_status(None)
                self.open_document()
            elif choice == 'n':
                pass
            else:
                print '''Please enter 'y' or 'n'. \n'''

    def save_document(self):
        if self.document.check_status():
            if self.document.status == 'Opened':
                choice = raw_input('Do you want to rewrite the opened file? (y/n)\n').lower()
                if choice == 'y':
                    pass
                elif choice == 'n':
                    self.document.get_name()
                else:
                    print '''Please enter 'y' or 'n'. \n'''
                    self.save_document()
            elif self.document.status == 'New':
                self.document.get_name()

            figures_information = []
            for i in xrange(len(self.canvas.figures)):
                figures_information.append(self.canvas.figures[i].give_information())
            self.document.save_file(self.canvas.window_width, self.canvas.window_height, self.canvas.window_color,
                                    figures_information)
        else:
            print 'Please create a new document or open the existing one\n'

    def add_figure(self):
        if self.document.check_status():
            figure_type = raw_input('Please enter the type of the figure: \n'
                                    'c - circle\n'
                                    'r - regular polygon\n'
                                    'i - irregular polygon\n').lower()
            if figure_type == 'c':
                figure = figures.Circle()
            elif figure_type == 'r':
                figure = figures.RPolygon()
            elif figure_type == 'i':
                figure = figures.IPolygon()
            else:
                figure = None
                print 'Try again!'
            if figure is not None:
                self.post_add_figure(figure)
        else:
            print 'Please create a new document or open the existing one\n'

    def post_add_figure(self, figure):
        figure.change_parameters()
        self.canvas.add_figure(figure)

    def del_figure(self):
        if self.document.check_status():
            figure_list = self.canvas.get_figures()
            if figure_list:
                figure_number = int(raw_input('Please enter the number of the figure: \n')) - 1
                if validity_check.Validation.existence_check(self.canvas, figure_number + 1):
                    del self.canvas.figures[figure_number]
                else:
                    print 'There is no such figure! Try again!'
        else:
            print 'Please create a new document or open the existing one\n'

    def get_figure_list(self):
        if self.document.status is not None:
            self.canvas.get_figures()
        else:
            print 'Please create a new document or open the existing one\n'

    def change_figure_parameters(self):
        if self.document.check_status():
            figure_list = self.canvas.get_figures()
            if figure_list:
                figure_number = int(raw_input('Please enter the number of the figure: \n')) - 1
                if validity_check.Validation.existence_check(self.canvas, figure_number + 1):
                    self.canvas.figures[figure_number].change_parameters()
                else:
                    print 'There is no such figure! Try again!'
        else:
            print 'Please create a new document or open the existing one\n'

    def build_image(self):
        if self.document.check_status():
            self.canvas.set_color()
            wn = turtle.Screen()
            wn.update()
            tu = turtle.Turtle()
            wn.setup(self.canvas.window_width, self.canvas.window_height)
            try:
                wn.bgcolor(self.canvas.window_color)
            except TurtleError:
                print 'Bad color!'
                self.canvas.window_color = 'white'
            finally:
                tu.speed(0)
                for i in xrange(len(self.canvas.figures)):
                    self.canvas.figures[i].draw_figure(tu)
                tu.hideturtle()
                wn.exitonclick()
        else:
            print 'Please create a new document or open the existing one\n'

    def menu(self, command):
        if command in self.commands.keys():
            self.commands[command]()
            self.choice()
        elif command == '9' or command == 'nine':
            print 'Bye-bye!'
        else:
            print 'Try again'
            self.choice()

    def run(self):
        self.choice()
