__author__ = 'Polina'

import gtk

class Validation(object):

    @staticmethod
    def is_correct(width, height):
        return True if 1 <= width <= gtk.gdk.screen_width() and 1 <= height <= gtk.gdk.screen_height() else False

    @staticmethod
    def is_in_area(x, y):
        return True if abs(x) <= gtk.gdk.screen_width()/2 and abs(y) <= gtk.gdk.screen_height()/2 else False

    @staticmethod
    def length_check(length):
        return True if length >= 1 else False

    @staticmethod
    def angle_check(angle_number):
        return True if 2 < angle_number < 12 else False

    @staticmethod
    def existence_check(canvas, figure_number):
        return True if 0 < figure_number <= len(canvas.figures) else False
