$(document).ready(function () {

    loadSelects();
    $("ul.tree").hide();
    var $treeTogglerLabel = $('label.tree-toggler');
    var $allCategoriesLabel = $('label.all-categories');
    $treeTogglerLabel.click(function () {
        $(this).parent().children('ul.tree').toggle(300);
    });

    $("li").parent("ul").parent("li").css( "list-style-type", 'disc');


    $treeTogglerLabel.mouseenter(function () {
    $(this).children('a.tree-toggler').css("color", "#6b6b6b");
    });

    $treeTogglerLabel.mouseleave(function () {
    $(this).children('a.tree-toggler').css("color", "#fff");
    });


    $allCategoriesLabel.mouseenter(function(){
        $(this).children('a.tree-toggler').css("color", "#6b6b6b");
    });

        $allCategoriesLabel.mouseleave(function(){
            $(this).children('a.tree-toggler').css("color", "#fff");
    })

});

    function loadingNote(id){
         $.get('/get_note/' + id, {}, function(data){

             $('#editNoteContainer').html(data);
                loadSelects();

        });
    }

function loadSelects(){
    $(".js-example-tags").select2({
        tags: true
    });

    $(".js-example-basic-multiple").select2();

    $(".js-example-basic-single").select2({
        minimumResultsForSearch: Infinity
    });

    $(".js-example-responsive").select2();



}