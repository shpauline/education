1. Создать виртуальное окружение: virtualenv .env
2. Войти в виртуальное окружение: source .env/bin/activate
3. Установить зависимости: pip install -r requirements.txt
4. Для загрузки начальных данных: ./manage.py loaddata initial_data.json
