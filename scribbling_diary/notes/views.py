from django.views.generic.edit import FormView
from django.contrib.auth.forms import AuthenticationForm
from django.views.generic.base import *
from django.contrib.auth import logout, login, authenticate
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.db.models import Q

from notes.forms import *


class ContextMixin(object):
    @classmethod
    def get_context(cls, request, context):
        context['tags'] = Tag.objects.all()
        context['categories'] = Category.objects.all()
        context['colors'] = Color.objects.all()
        context['category_choosing'] = Category.objects.exclude(parent=None)
        context['users'] = Account.objects.exclude(username=request.user.username).exclude(is_superuser=True)


class LoginRequiredMixin(object):
    @classmethod
    def as_view(cls, **initkwargs):
        view = super(LoginRequiredMixin, cls).as_view(**initkwargs)
        return login_required(view)


class RegisterUserView(FormView):
    form_class = MyRegistrationForm

    template_name = "notes/register.html"

    success_url = "/profile"

    def form_valid(self, form):
        form.save()
        username = self.request.POST['username']
        password = self.request.POST['password1']
        user = authenticate(username=username, password=password)
        login(self.request, user)
        return super(RegisterUserView, self).form_valid(form)


class LoginFormView(FormView):
    form_class = AuthenticationForm
    template_name = "notes/login.html"
    success_url = "/profile"

    def form_valid(self, form):
        self.user = form.get_user()
        login(self.request, self.user)
        return super(LoginFormView, self).form_valid(form)


class LogoutView(View):
    @staticmethod
    def get(request):
        logout(request)
        return HttpResponseRedirect("/", {})


class IndexPageView(TemplateView):
    template_name = "notes/index.html"

    def get_context_data(self, **kwargs):
        context = super(IndexPageView, self).get_context_data(**kwargs)
        context['images'] = [[str(i + 3 * (j - 1)) for i in xrange(1, 4)] for j in xrange(1, 4)]
        titles = ['Explore yourself', 'Enjoy yourself', 'Express yourself']
        for i, title in enumerate(titles):
            context['images'][i].insert(0, title)
        return context


class ProfilePageView(LoginRequiredMixin, ContextMixin, TemplateView):
    template_name = 'notes/profile.html'

    def get_context_data(self, **kwargs):
        context = super(ProfilePageView, self).get_context_data(**kwargs)
        user_notes = Note.objects.filter(permission=self.request.user, note_account_permissions__access='A')
        self.get_context(self.request, context)
        context['notes'] = user_notes
        if len(user_notes) > 0:
            context['note'] = user_notes[0]
            context['readers'] = Account.objects.filter(note=user_notes[0], permission__access='R')
        else:
            context['note'] = None
            context['readers'] = None
        return context


class AddNoteView(FormView):
    form_class = NoteForm
    template_name = 'notes/profile.html'
    success_url = "/profile"

    def form_valid(self, form):
        note = form.save()

        Permission.objects.create(user=self.request.user, note=note, access='A')
        tag_list = self.request.POST.getlist('tags')

        for tag in tag_list:
            new_tag = Tag.objects.get_or_create(title=tag)
            note.tags.add(new_tag[0])

        readers_list = self.request.POST.getlist('readers')
        for person in readers_list:
            reader = Account.objects.get(id=person)
            Permission.objects.create(user=reader, note=note, access='R')
        return super(AddNoteView, self).form_valid(form)

    def form_invalid(self, form):
        return HttpResponseRedirect('/profile', {})


class DeleteNoteView(View):
    template_name = 'notes/profile.html'

    @staticmethod
    def post(request):
        note_list = request.POST.getlist('note')
        for note in note_list:
            Note.objects.filter(id=note).delete()
        return HttpResponseRedirect('/profile', {})


class EditNoteView(FormView):
    template_name = 'notes/profile.html'
    success_url = "/profile"

    def form_valid(self, form):
        note = form.save()
        for category in note.categories.all():
            note.categories.remove(category)
        note.categories.add(*self.request.POST.getlist('categories'))

        for tag in note.tags.all():
            note.tags.remove(tag)
        tag_list = self.request.POST.getlist('tags')
        for tag in tag_list:
            new_tag = Tag.objects.get_or_create(title=tag)
            note.tags.add(new_tag[0])

        note.pub_date = timezone.now()

        Permission.objects.filter(note=note, access='R').delete()
        readers = self.request.POST.getlist('readers')
        for person in readers:
            reader = Account.objects.get(id=person)
            Permission.objects.create(user=reader, note=note, access='R')
        return super(EditNoteView, self).form_valid(form)

    def form_invalid(self, form):
        return HttpResponseRedirect('/profile', {})

    def get_form(self, form_class=None):
        note_id = self.request.POST['note']
        note = Note.objects.get(id=note_id)
        form = NoteForm(self.request.POST, self.request.FILES, instance=note)
        return form


class CategorySearchView(ContextMixin, TemplateView):
    template_name = 'notes/search_notes.html'

    def get(self, request, **kwargs):
        user_notes = Note.objects.filter(Q(permission=request.user), Q(note_account_permissions__access='A') |
                                         Q(note_account_permissions__access='R'))
        result_notes = []
        for note in user_notes:
            note_categories = Category.objects.filter(note=note)
            for current_category in note_categories:
                self.search_categories(note=note, current_category=current_category, category=kwargs['category'],
                                       result_notes=result_notes)
        context = dict()
        self.get_context(request, context)
        context['notes'] = result_notes
        if kwargs['category'] == 'all':
            context['notes'] = user_notes
        return self.render_to_response(context)

    def search_categories(self, **kwargs):
        current_category = kwargs['current_category']
        category = kwargs['category']
        note = kwargs['note']
        result_notes = kwargs['result_notes']
        if str(current_category) == str(category) and note not in result_notes:
            result_notes.append(note)
        if current_category.parent is None:
            return result_notes
        else:
            return self.search_categories(note=note, current_category=current_category.parent, category=category,
                                          result_notes=result_notes)


class TagSearchView(ContextMixin, TemplateView):
    template_name = 'notes/search_notes.html'

    def get(self, request, **kwargs):
        user_notes = Note.objects.filter(Q(permission=request.user), Q(note_account_permissions__access='A') |
                                         Q(note_account_permissions__access='R'))
        result_notes = []
        for note in user_notes:
            note_tags = Tag.objects.filter(note=note)
            for current_tag in note_tags:
                if str(current_tag) == str(kwargs['tag']) and current_tag not in result_notes:
                    result_notes.append(note)
        context = dict()
        self.get_context(request, context)
        context['notes'] = result_notes
        return self.render_to_response(context)


class GetNoteView(ContextMixin, TemplateView):
    template_name = 'notes/load_note.html'

    def get_context_data(self, **kwargs):
        note = Note.objects.get(id=kwargs['note_id'])
        context = super(GetNoteView, self).get_context_data(**kwargs)
        self.get_context(self.request, context)
        context['note'] = note
        context['readers'] = Account.objects.filter(note=note, permission__access='R')
        return context


class IReadView(LoginRequiredMixin, ContextMixin, TemplateView):
    template_name = 'notes/i_read.html'

    def get_context_data(self, **kwargs):
        all_notes = Note.objects.filter(permission=self.request.user, note_account_permissions__access='R')
        context = super(IReadView, self).get_context_data(**kwargs)
        self.get_context(self.request, context)
        context['notes'] = all_notes
        context['authors'] = []
        for note in all_notes:
            context['authors'].append(
                {'note': note.id, 'author': Account.objects.get(note=note, permission__access='A').username})
        return context


class DefineFriendsView(View):
    @staticmethod
    def post(request):
        user = Account.objects.get(username=request.user)
        Friend.objects.filter(user=request.user).delete()
        new_friend_list = request.POST.getlist('friends')
        for person in new_friend_list:
            friend = Account.objects.get(id=person)
            Friend.objects.create(user=user, friend=friend)
        return HttpResponseRedirect('/profile', {})


class SearchNotesView(LoginRequiredMixin, ContextMixin, View):
    template_name = 'notes/search_notes.html'

    def post(self, request):
        context = dict()
        self.get_context(request, context)
        search_result = []
        user_note_check = request.POST.getlist('user_notes_check')
        read_note_check = request.POST.getlist('read_notes_check')

        if len(user_note_check) > 0 >= len(read_note_check):
            notes = Note.objects.filter(permission=request.user, note_account_permissions__access="A")

        elif len(read_note_check) > 0 >= len(user_note_check):
            notes = Note.objects.filter(permission=request.user, note_account_permissions__access="R")

        else:
            notes = Note.objects.filter(Q(permission=request.user), Q(note_account_permissions__access='A') |
                                        Q(note_account_permissions__access='R'))

        keyword_array = request.POST.getlist('keywords_input')
        if len(keyword_array) > 0:
            keywords = str(keyword_array[0]).split()
            for keyword in keywords:
                for note in notes:
                    if keyword.lower() in note.title.lower() or keyword in note.text.lower():
                        if note not in search_result:
                            search_result.append(note)

            context['notes'] = search_result
        else:
            context['notes'] = None
        return render(request, self.template_name, context)
