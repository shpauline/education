from django.contrib import admin
from django_mptt_admin.admin import DjangoMpttAdmin
from django.contrib.auth.models import Group

from notes.forms import *
from notes.models import *


class AccountAdmin(admin.ModelAdmin):
    model = Account
    fieldsets = [
        ('Name', {'fields': ['username']}),
        ('Tagline', {'fields': ['tagline'], 'classes': ('collapse',)}),
        ('Account information', {'fields': ['is_superuser', 'is_staff', 'last_login'], 'classes': ('collapse',)}),
    ]


class PermissionAdmin(admin.ModelAdmin):
    model = Permission


class FriendAdmin(admin.ModelAdmin):
    model = Friend


class TagAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Title', {'fields': ['title']}),
    ]


class CategoryAdmin(DjangoMpttAdmin):
    pass


class ColorAdmin(admin.ModelAdmin):
    form = ColorForm


class PermissionInline(admin.StackedInline):
    classes = 'collapse'
    model = Permission
    extra = 1


class NoteAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Title', {'fields': ['title']}),
        ('Time information', {'fields': ['pub_date']}),
        ('Content', {'fields': ['text', 'tags', 'categories'], 'classes': ('collapse',), }),
        ('Color information', {'fields': ['background_color', 'text_color'], 'classes': ('collapse',)})
    ]
    filter_horizontal = ['tags', 'categories']
    list_filter = ['categories']
    search_fields = ['title']
    inlines = [PermissionInline]


admin.site.unregister(Group)
admin.site.register(Account, AccountAdmin)
admin.site.register(Color, ColorAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(Note, NoteAdmin)
admin.site.register(Permission, PermissionAdmin)
admin.site.register(Friend, FriendAdmin)
