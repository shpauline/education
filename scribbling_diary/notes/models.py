from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
import mptt
from django.utils import timezone

white_color = '#FFFFFF'
black_color = '#000000'


class AccountManager(BaseUserManager):
    use_in_migrations = True

    def create_user(self, password=None, **kwargs):
        if not kwargs.get('username'):
            raise ValueError('User must have a valid username.')

        account = self.model(username=kwargs.get('username'))

        account.set_password(password)
        account.save()

        return account

    def create_superuser(self, password, **kwargs):
        account = self.create_user(password, **kwargs)

        account.is_staff = True
        account.is_superuser = True
        account.save()

        return account


class Account(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=40, unique=True)
    is_staff = models.BooleanField(default=False)
    account_friends = models.ManyToManyField('self', blank=True, through='Friend', symmetrical=False)

    objects = AccountManager()

    USERNAME_FIELD = 'username'

    def __unicode__(self):
        return self.username

    def get_full_name(self):
        return self.username

    def get_short_name(self):
        return self.username


class Category(models.Model):
    class Meta:
        verbose_name_plural = "categories"

    title = models.CharField('Title', max_length=100)
    parent = models.ForeignKey('self', blank=True, null=True, verbose_name="parent", related_name='child')

    def __unicode__(self):
        return self.title


mptt.register(Category, )


class Tag(models.Model):
    title = models.CharField(max_length=50)

    def __unicode__(self):
        return self.title


class Color(models.Model):
    title = models.CharField('Title', max_length=30)
    value = models.CharField('Value', max_length=7, default=white_color, unique=True)

    def __unicode__(self):
        return self.title


class Note(models.Model):
    title = models.CharField('Title', max_length=100)
    text = models.TextField('Text', max_length=1500, blank=True)
    tags = models.ManyToManyField(Tag, blank=True)
    categories = models.ManyToManyField(Category, blank=True)
    background_color = models.ForeignKey(Color, blank=True, related_name='note_background_color')
    text_color = models.ForeignKey(Color, blank=True, related_name='note_text_color')
    pub_date = models.DateTimeField('date published', default=timezone.now)
    permission = models.ManyToManyField(Account, through='Permission')
    image = models.ImageField(blank=True, upload_to='pictures_folder/')

    def __unicode__(self):
        return self.title

    def get_background_color(self):
        return white_color if self.background_color is None else self.background_color

    def get_text_color(self):
        return black_color if self.text_color is None else self.text_color


class Permission(models.Model):
    user = models.ForeignKey(Account)
    note = models.ForeignKey(Note, related_name='note_account_permissions')

    ACCESS_CHOICES = (
        ('A', 'Author'),
        ('R', 'Reader'),
        ('W', 'Writer'),
    )
    access = models.CharField(max_length=1, choices=ACCESS_CHOICES, default='A')

    def __unicode__(self):
        return self.note.title


class Friend(models.Model):
    user = models.ForeignKey(Account)
    friend = models.ForeignKey(Account, related_name='user_friend')
