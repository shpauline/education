from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings

from . import views


urlpatterns = [
    url(r'^$', views.IndexPageView.as_view(), name='index'),
    url(r'^register/$', views.RegisterUserView.as_view(), name='register'),
    url(r'^profile/$', views.ProfilePageView.as_view(), name='profile'),
    url(r'^add_note/$', views.AddNoteView.as_view(), name='add_note'),
    url(r'^del_note/$', views.DeleteNoteView.as_view(), name='del_note'),
    url(r'^edit_note/$', views.EditNoteView.as_view(), name='edit_note'),
    url(r'^category_search/(?P<category>\w+)/$', views.CategorySearchView.as_view(),
        name='category_search'),
    url(r'^tag_search/(?P<tag>\w+)/$', views.TagSearchView.as_view(), name='tag_search'),
    url(r'^get_note/(?P<note_id>[0-9]+)/$', views.GetNoteView.as_view(), name='get_note'),
    url(r'^i_read/$', views.IReadView.as_view(), name='i_read'),
    url(r'^define_friends/$', views.DefineFriendsView.as_view(), name='define_friends'),
    url(r'^search_notes/$', views.SearchNotesView.as_view(), name='search_notes'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()
