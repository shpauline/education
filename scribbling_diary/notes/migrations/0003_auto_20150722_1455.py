# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('notes', '0002_permission'),
    ]

    operations = [
        migrations.AddField(
            model_name='note',
            name='permission',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, through='notes.Permission'),
        ),
        migrations.AlterField(
            model_name='permission',
            name='note',
            field=models.ForeignKey(related_name='note_account_permissions', to='notes.Note'),
        ),
    ]
