# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('notes', '0005_auto_20150806_1116'),
    ]

    operations = [
        migrations.CreateModel(
            name='Friends',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='account',
            name='friends',
        ),
        migrations.AddField(
            model_name='friends',
            name='friend',
            field=models.ForeignKey(related_name='user_friend', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='friends',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='account',
            name='account_friends',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, through='notes.Friends', blank=True),
        ),
    ]
