# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notes', '0003_auto_20150722_1455'),
    ]

    operations = [
        migrations.AlterField(
            model_name='permission',
            name='access',
            field=models.CharField(default=b'A', max_length=1, choices=[(b'A', b'Author'), (b'R', b'Reader'), (b'W', b'Writer')]),
        ),
    ]
