from django import forms
from django.contrib.auth.forms import UserCreationForm

from notes.models import *


class ColorForm(forms.ModelForm):
    value = forms.RegexField(regex='[#][a-fA-F0-9]+')


class MyRegistrationForm(UserCreationForm):
    class Meta:
        model = Account
        fields = ('username', 'password1', 'password2')

    def save(self, commit=True):
        user = super(MyRegistrationForm, self).save(commit=False)

        if commit:
            user.save()
        return user


class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        exclude = ['permission', 'tags', 'pub_date']
