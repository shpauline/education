__author__ = 'Polina'

import gtk
import os
import sys
from werkzeug.wrappers import Request, Response
from werkzeug.routing import Map, Rule
from werkzeug.exceptions import HTTPException
from werkzeug.wsgi import SharedDataMiddleware
from jinja2 import Environment, FileSystemLoader

from fractal_flame.application import *

class FractalApplication(object):
    def __init__(self):
        self.x_res = self.y_res = self.seed = self.iterations = self.transformation_type = self.samples = 0
        self.wsgi_app = SharedDataMiddleware(self.wsgi_app, {
            '/static': os.path.join(os.path.dirname(__file__), 'static')
        })

        template_path = os.path.join(os.path.dirname(__file__), 'templates')
        self.jinja_env = Environment(loader=FileSystemLoader(template_path),
                                     autoescape=True)

        self.url_map = Map([
            Rule('/', endpoint='main'),
            Rule('/draw_image', endpoint='draw_image')
        ])

    def __call__(self, environ, start_response):
        return self.wsgi_app(environ, start_response)

    def wsgi_app(self, environ, start_response):
        request = Request(environ)
        response = self.dispatch_request(request)
        return response(environ, start_response)

    def dispatch_request(self, request):
        adapter = self.url_map.bind_to_environ(request.environ)
        try:
            endpoint, values = adapter.match()
            return getattr(self, 'on_' + endpoint)(request, **values)
        except HTTPException, e:
            return e

    def render_template(self, template_name, **context):
        t = self.jinja_env.get_template(template_name)
        return Response(t.render(context), mimetype='text/html')

    def on_main(self, request):
        return self.render_template('main.html')

    def on_draw_image(self, request):
        if request.method == 'POST':
            try:
                self.get_parameters(request)
            except ValueError:
                error = 'Please enter a valid data'
                return self.render_template('main.html', error=error)

            else:
                app = Application()
                app.canvas = canvas.Canvas(x_res=self.x_res, y_res=self.y_res, samples=self.samples,
                                           iterations=self.iterations, seed=self.seed)
                app.randomize = random.Random(self.seed)
                app.choice = self.transformation_type
                app.coefficient_initialization()
                app.generation()
                app.gamma_conversion()
                app.draw_image()
                return self.render_template('image.html', image='/static/images/' + app.file_name)

    def get_parameters(self, request):
        self.x_res = int(request.form['x_res'])
        self.y_res = int(request.form['y_res'])
        self.samples = int(request.form['samples'])
        self.iterations = int(request.form['iterations'])
        self.seed = request.form['seed']
        self.transformation_type = int(request.form['transformation_type'])
        if not self.is_valid_data(x_res=self.x_res, y_res=self.y_res, samples=self.samples, iterations=self.iterations):
            raise ValueError

    @staticmethod
    def is_valid_data(**kwargs):
        return 1 <= kwargs['x_res'] <= gtk.gdk.screen_width() and 1 <= kwargs['y_res'] <= gtk.gdk.screen_height() and \
            kwargs['iterations'] >= 1


if __name__ == '__main__':
    from werkzeug.serving import run_simple

    application = FractalApplication()
    run_simple('127.0.0.1', 3000, application)
