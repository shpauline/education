__author__ = 'Polina'

import random

limit_value = 1.5
max_color = 255
start_index = 1

class Coefficient(object):

    random_range = (-limit_value, limit_value)
    coefficient_array = 'abcdef'
    channel_array = ['r_channel', 'b_channel', 'g_channel']

    def __init__(self, **kwargs):

        self.generate_coefficients()
        self.check_coefficients()
        self.index = 0
        for parameter in kwargs:
            self.__setattr__(parameter, kwargs[parameter])

    def generate_coefficients(self):
        for coefficient in self.coefficient_array:
            self.__setattr__(coefficient, random.uniform(*self.random_range))
        if self.check_coefficients():
            for channel in self.channel_array:
                self.__setattr__(channel, random.randrange(max_color + 1))
        else:
            self.generate_coefficients()

    def check_coefficients(self):
        return self.a**2 + self.b**2 + self.d**2 + self.e**2 < 1 + (self.a*self.e - self.b*self.d)**2 and self.a**2 + \
            self.d**2 < 1 and self.b**2 + self.e**2 < 1

    def give_parameters(self):
        return {'index': self.index,
                'a': self.a,
                'b': self.b,
                'c': self.c,
                'd': self.d,
                'e': self.e,
                'f': self.f,
                'r_channel': self.r_channel,
                'g_channel': self.g_channel,
                'b_channel': self.b_channel
                }
