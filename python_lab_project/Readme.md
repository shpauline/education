1. Создать виртуальное окружение: virtualenv .env
2. Войти в виртуальное окружение: source .env/bin/activate
3. Установить зависимости: pip install -r requirements.txt
4. Применить миграции: ./manage.py migrate
5. Для загрузки начальных данных: ./manage.py loaddata fixtures.json
6. Для установки bower-компонентов: bower install
7. Запуск: ./manage.py runserver
