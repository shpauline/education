from django.contrib import admin

from python_lab_app import models


class MarkInline(admin.TabularInline):
    classes = 'collapse'
    model = models.Applicant.mark.through
    extra = 0

    verbose_name = "Mark"
    verbose_name_plural = "Marks"


class MarkAdmin(admin.ModelAdmin):
    pass
admin.site.register(models.Mark, MarkAdmin)


class ApplicantAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['name', 'surname']}),
        (None, {'fields': ['email']}),
        (None, {'fields': ['age']}),
        ('Additional', {'fields': ['is_student', 'is_job_applicant']})
    ]
    list_display = ('application_data', 'surname', 'name', 'email', 'age',
                    'is_student', 'is_job_applicant')
    list_display_links = ('surname', )
    search_fields = ('surname', )
    list_filter = ('is_student', 'is_job_applicant', )

    inlines = [MarkInline, ]


class PointAdmin(admin.ModelAdmin):
    list_display = ('title', 'subcategory', )
    list_filter = ('subcategory', )


class SubcategoryInLine(admin.StackedInline):
    model = models.Subcategory


class CategoryAdmin(admin.ModelAdmin):
    model = models.Category
    inlines = (SubcategoryInLine,)


admin.site.register(models.Applicant, ApplicantAdmin)
admin.site.register(models.Point, PointAdmin)
admin.site.register(models.Category, CategoryAdmin)
admin.site.register(models.Paragraph)
admin.site.register(models.Skill)
admin.site.register(models.Recipient)
admin.site.register(models.Message)


