from django.db import models
from mezzanine.core.models import RichTextField
import django.utils.timezone


class Skill(models.Model):
    title = models.CharField(max_length=50, unique=True)

    def __unicode__(self):
        return self.title


class Mark(models.Model):
    title = models.ForeignKey('Skill')
    value = models.PositiveIntegerField(default=1)

    def __unicode__(self):
        return self.title.title + ': ' + str(self.value)


class Applicant(models.Model):
    name = models.CharField(max_length=30, verbose_name='First name')
    surname = models.CharField(max_length=30, verbose_name='Last name')
    email = models.EmailField(max_length=50, verbose_name='E-mail address')
    phone_number = models.CharField(max_length=20, verbose_name='Phone number')
    age = models.PositiveIntegerField()
    mark = models.ManyToManyField('Mark')
    is_student = models.BooleanField(verbose_name='Student')
    is_job_applicant = models.BooleanField(verbose_name='Job applicant')
    application_data = models.DateTimeField(default=django.utils.timezone.now)

    def __unicode__(self):
        return self.name + ' ' + self.surname


class Category(models.Model):
    class Meta:
        verbose_name_plural = "categories"

    title = models.CharField(max_length=100, unique=False, )

    def __unicode__(self):
        return self.title


class Subcategory(models.Model):
    title = models.CharField(max_length=100, unique=False)
    category = models.ForeignKey("Category")

    def __unicode__(self):
        return self.title


class Point(models.Model):
    title = models.TextField(max_length=300, unique=False)
    subcategory = models.ForeignKey("Subcategory", blank=True, null=True)

    def __unicode__(self):
        return self.title


class Paragraph(models.Model):
    title = models.CharField(max_length=50)
    text = RichTextField()

    def __unicode__(self):
        return self.title


class Recipient(models.Model):
    email = models.EmailField(max_length='50',  verbose_name='E-mail address', unique=True, null=False)

    def __unicode__(self):
        return self.email


class Message(models.Model):
    title = models.CharField(unique=True, max_length=100)
    text = RichTextField()

    def __unicode__(self):
        return self.title
