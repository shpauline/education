# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('python_lab_app', '0002_auto_20150814_0855'),
    ]

    operations = [
        migrations.AlterField(
            model_name='paragraph',
            name='title',
            field=models.CharField(max_length=50),
        ),
    ]
