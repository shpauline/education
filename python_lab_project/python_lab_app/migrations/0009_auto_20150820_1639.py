# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('python_lab_app', '0008_auto_20150820_1623'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recipient',
            name='email',
            field=models.EmailField(unique=True, max_length=b'50', verbose_name=b'E-mail address'),
        ),
    ]
