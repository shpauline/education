# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('python_lab_app', '0006_auto_20150820_1621'),
    ]

    operations = [
        migrations.AlterField(
            model_name='applicant',
            name='application_data',
            field=models.DateTimeField(default=datetime.datetime(2015, 8, 20, 13, 22, 47, 115867, tzinfo=utc)),
        ),
    ]
