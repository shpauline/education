# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('python_lab_app', '0010_auto_20150821_0920'),
    ]

    operations = [
        migrations.AlterField(
            model_name='applicant',
            name='phone_number',
            field=models.CharField(max_length=20, verbose_name=b'Phone number'),
        ),
    ]
