# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('python_lab_app', '0009_auto_20150820_1639'),
    ]

    operations = [
        migrations.AlterField(
            model_name='applicant',
            name='phone_number',
            field=models.CharField(max_length=18, verbose_name=b'Phone number'),
        ),
    ]
