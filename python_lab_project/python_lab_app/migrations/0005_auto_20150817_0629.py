# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('python_lab_app', '0004_auto_20150814_0932'),
    ]

    operations = [
        migrations.CreateModel(
            name='Mark',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.PositiveIntegerField(default=1)),
            ],
        ),
        migrations.CreateModel(
            name='Skill',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=50)),
            ],
        ),
        migrations.RemoveField(
            model_name='applicant',
            name='css_mark',
        ),
        migrations.RemoveField(
            model_name='applicant',
            name='english_mark',
        ),
        migrations.RemoveField(
            model_name='applicant',
            name='html_mark',
        ),
        migrations.RemoveField(
            model_name='applicant',
            name='js_mark',
        ),
        migrations.RemoveField(
            model_name='applicant',
            name='web_mark',
        ),
        migrations.AlterField(
            model_name='applicant',
            name='application_data',
            field=models.DateTimeField(default=datetime.datetime(2015, 8, 17, 6, 29, 7, 219109, tzinfo=utc)),
        ),
        migrations.AddField(
            model_name='mark',
            name='title',
            field=models.ForeignKey(to='python_lab_app.Skill'),
        ),
        migrations.AddField(
            model_name='applicant',
            name='mark',
            field=models.ManyToManyField(to='python_lab_app.Mark'),
        ),
    ]
