# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import mezzanine.core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('python_lab_app', '0012_message'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='text',
            field=mezzanine.core.fields.RichTextField(),
        ),
    ]
