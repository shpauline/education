# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('python_lab_app', '0005_auto_20150817_0629'),
    ]

    operations = [
        migrations.CreateModel(
            name='Recipient',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(max_length=b'50', verbose_name=b'E-mail address')),
            ],
        ),
        migrations.AlterField(
            model_name='applicant',
            name='application_data',
            field=models.DateTimeField(default=datetime.datetime(2015, 8, 20, 13, 21, 47, 766738, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='skill',
            name='title',
            field=models.CharField(unique=True, max_length=50),
        ),
    ]
