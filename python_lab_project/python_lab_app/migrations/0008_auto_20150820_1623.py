# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('python_lab_app', '0007_auto_20150820_1622'),
    ]

    operations = [
        migrations.AlterField(
            model_name='applicant',
            name='application_data',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
