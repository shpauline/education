# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('python_lab_app', '0003_auto_20150814_0855'),
    ]

    operations = [
        migrations.AddField(
            model_name='applicant',
            name='application_data',
            field=models.DateTimeField(default=datetime.datetime(2015, 8, 14, 9, 32, 56, 662297, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='point',
            name='subcategory',
            field=models.ForeignKey(blank=True, to='python_lab_app.Subcategory', null=True),
        ),
        migrations.AlterField(
            model_name='subcategory',
            name='category',
            field=models.ForeignKey(to='python_lab_app.Category'),
        ),
    ]
