# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import mezzanine.core.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Applicant',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30, verbose_name=b'First name')),
                ('surname', models.CharField(max_length=30, verbose_name=b'Second name')),
                ('email', models.EmailField(max_length=50, verbose_name=b'E-mail address')),
                ('phone_number', models.CharField(max_length=10, verbose_name=b'Phone number')),
                ('age', models.PositiveIntegerField()),
                ('english_mark', models.PositiveIntegerField(default=1)),
                ('html_mark', models.PositiveIntegerField(default=1)),
                ('css_mark', models.PositiveIntegerField(default=1)),
                ('js_mark', models.PositiveIntegerField(default=1)),
                ('web_mark', models.PositiveIntegerField(default=1)),
                ('is_student', models.BooleanField(verbose_name=b'Student')),
                ('is_job_applicant', models.BooleanField(verbose_name=b'Job applicant')),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'categories',
            },
        ),
        migrations.CreateModel(
            name='Paragraph',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', mezzanine.core.fields.RichTextField()),
            ],
        ),
        migrations.CreateModel(
            name='Point',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.TextField(max_length=300)),
            ],
        ),
        migrations.CreateModel(
            name='Subcategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100)),
                ('category', models.ForeignKey(related_name='subcategory_category', to='python_lab_app.Category')),
            ],
        ),
        migrations.AddField(
            model_name='point',
            name='subcategory',
            field=models.ForeignKey(related_name='point_subcategory', to='python_lab_app.Subcategory'),
        ),
    ]
