jQuery(document).ready(function () {

    $(window).scrollTop(0);
    $('#toElemButton').effect("bounce", {times: 5}, 300);
    $('#toElem').click(
        function () {


            var $elem = $('#header');
            $('html, body').animate({scrollTop: $elem.height() + $elem.height() / 100}, 1000);
        }
    );

    $('#toHome').click(
        function () {
            $('html, body').animate({scrollTop: 0}, 1000);
        }
    );
    $('#toRegistrationForm').click(
        function () {
            var $target = $('#registrationForm');
            var $sticker = $('#sticker');
            $('html, body').animate({scrollTop: $target.offset().top - $sticker.height()}, 1000);
        }
    );


    var offset = 1000;
    var duration = 500;
    var scrollButton = jQuery('.back-to-top');
    scrollButton.fadeOut(duration);
    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() > offset) {
            scrollButton.fadeIn(duration);
        } else {
            scrollButton.fadeOut(duration);
        }

    });

    scrollButton.click(function (event) {
        event.preventDefault();
        jQuery('html, body').animate({scrollTop: 0}, duration);
        return false;
    });

    $("#sticker").sticky({topSpacing: 0, className: "hey"});


    var sliderInput = jQuery('.slider-input');
    for (var i = 0; i < sliderInput.length; i++) {

        new Slider('#' + sliderInput[i].id, {
            formatter: function (value) {
                return 'Current value: ' + value;
            }
        });
    }

    (function () {
        // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
        if (!String.prototype.trim) {
            (function () {
                // Make sure we trim BOM and NBSP
                var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                String.prototype.trim = function () {
                    return this.replace(rtrim, '');
                };
            })();
        }

        [].slice.call(document.querySelectorAll('input.input__field')).forEach(function (inputEl) {
            // in case the input is already filled..
            if (inputEl.value.trim() !== '') {
                classie.add(inputEl.parentNode, 'input--filled');
            }

            // events:
            inputEl.addEventListener('focus', onInputFocus);
            inputEl.addEventListener('blur', onInputBlur);
        });

        function onInputFocus(ev) {
            classie.add(ev.target.parentNode, 'input--filled');
        }

        function onInputBlur(ev) {
            if (ev.target.value.trim() === '') {
                classie.remove(ev.target.parentNode, 'input--filled');
            }
        }
    })();

    $("#id_tel").mask("(999) 999 - 99 - 99");

    setTimeout(function () {

        $('#SUCCESSFUL_REGISTRATION').hide("slow")
    }, 20000)

});

