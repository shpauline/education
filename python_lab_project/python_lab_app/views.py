from django.shortcuts import render
from django.views.generic.edit import FormView
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import render_to_response
from django.template import RequestContext

from python_lab_app import forms, models


class IndexPageView(FormView):
    template_name = "main.html"
    form_class = forms.ApplicantForm
    success_url = '/'

    def get_context_data(self, **kwargs):
        paragraphs = models.Paragraph.objects.all()
        categories = models.Category.objects.all()
        subcategories = models.Subcategory.objects.all()
        points = models.Point.objects.all()
        skills = models.Skill.objects.all().order_by('title')
        context = super(IndexPageView, self).get_context_data(**kwargs)
        context['paragraphs'] = paragraphs
        context['categories'] = categories
        context['subcategories'] = subcategories
        context['points'] = points
        context['skills'] = skills
        return context

    def form_valid(self, form):
        applicant = form.save()
        for key in form.data.keys():
            if key[:5] == 'mark_':
                skill = models.Skill.objects.get(title=key[5:])
                mark = models.Mark.objects.create(title=skill, value=int(form.data[key]))
                applicant.mark.add(mark)

        # html_template = get_template('email.html')
        # d = Context({'applicant': applicant, 'phone': applicant.phone_number,
        #              'email': applicant.email})
        #
        # html_content = html_template.render(d)
        # recipients = list()
        # for recipient in models.Recipient.objects.all():
        #     recipients.append(recipient.email)
        #
        # msg = EmailMultiAlternatives('Python Lab Application: ' + str(applicant), '', 'polyashinkarenko@gmail.com',
        #                              recipients)
        # msg.attach_alternative(html_content, "text/html")
        # msg.send()
        context = self.get_context_data()
        context['message'] = models.Message.objects.get(title='SUCCESSFUL_REGISTRATION')
        return render(self.request, self.template_name, context)


def handler404(request):
    response = render_to_response('404.html', {'message': models.Message.objects.get(title='NOT_FOUND')},
                                  context_instance=RequestContext(request))
    response.status_code = 404
    return response


def handler500(request):
    response = render_to_response('500.html', {'message': models.Message.objects.get(title='INTERNAL_SERVER_ERROR')},
                                  context_instance=RequestContext(request))
    response.status_code = 500
    return response

