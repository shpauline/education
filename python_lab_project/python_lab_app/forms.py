from django import forms

from python_lab_app import models


class ApplicantForm(forms.ModelForm):
    class Meta:
        model = models.Applicant
        exclude = ['application_data', 'mark', ]
